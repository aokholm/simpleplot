//
//  SimplePlot.swift
//
//  Created by Andreas Okholm on 11/08/15.
//
//

import Foundation
import UIKit

public func color(index: Int, total: Int) -> CGColor {
    return UIColor(hue: CGFloat(index)/CGFloat(total), saturation: 1, brightness: 1, alpha: 1).CGColor
}

public class SimplePlot: UIView {
    var shapeLayer: CAShapeLayer { return layer as! CAShapeLayer }
    var t: Affine!
    
    var axesMaxMinPathBox = CGRect(x: 0, y: 0, width: 0.00001, height: 0.00001)
    var pointss = [[CGPoint]]()
    var pathBox = CGRect()
    let inset = UIEdgeInsets(top: 0, left: 60, bottom: 30, right: 0)
    var myFrame = CGRect()
    
    var gridValuesY: [CGFloat] {
        return gridValuesForRange(pathBox.minY, max: pathBox.maxY)
    }
    
    var gridValuesX: [CGFloat] {
        return gridValuesForRange(pathBox.minX, max: pathBox.maxX)
    }
    
    var gridPathsY: [UIBezierPath] {
        return gridValuesY.map({[CGPoint(x: pathBox.minX, y: $0), CGPoint(x: pathBox.maxX, y: $0)]}).map(bezierPath)
    }
    
    var gridPathsX: [UIBezierPath] {
        return gridValuesX.map({[CGPoint(x: $0, y: pathBox.minY), CGPoint(x: $0, y: pathBox.maxY)]}).map(bezierPath)
    }
    
    public func display() {
        let paths = pointss.map(bezierPath)
        
        pathBox = paths.reduce(axesMaxMinPathBox) {
            CGRectUnion($0, $1.bounds)
        }
        
        myFrame = UIEdgeInsetsInsetRect(bounds, inset)
        
        t = Affine(from: pathBox, to: myFrame, flipped: true)
        
        //        let gridPaths = gridLinesForBox(pathBox)
        
        plotPaths(gridPathsX, pathColor: UIColor(white: 0.5, alpha: 1))
        plotPaths(gridPathsY, pathColor: UIColor(white: 0.5, alpha: 1))
        plotPaths(paths, pathColor: nil)
        
        
        _ = gridValuesY.map {self.uilabel($0, axisX: false)}.map(self.addSubview)
        _ = gridValuesX.map {self.uilabel($0, axisX: true)}.map(self.addSubview)
    }
    
    func plotPaths(paths: [UIBezierPath], pathColor: UIColor?) {
        for (index, path) in paths.enumerate() {
            let line = CAShapeLayer()
            line.fillColor = nil
            
            if let col = pathColor {
                line.strokeColor = col.CGColor
            }
            else {
                line.strokeColor = color(index, total: paths.count)
            }
            
            path.applyTransform(t!)
            line.path = path.CGPath
            shapeLayer.addSublayer(line)
        }
    }
    
    func uilabel(val: CGFloat, axisX: Bool) -> UILabel {
        let label = UILabel(frame: CGRect(origin: CGPoint(), size: CGSize(width: 60, height: 20)))
        label.minimumScaleFactor = 0.1
        label.adjustsFontSizeToFitWidth = true
        label.text = String(format: "%g", val)
        
        let q: CGPoint
        if axisX {
            let point = CGPoint(x: val, y: 0)
            q = CGPoint(x: t.apply(point).x, y: myFrame.maxY + 5)
            label.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
            label.textAlignment = .Center
        } else {
            let point = CGPoint(x: 0, y: val)
            q = CGPoint(x: myFrame.minX - 5, y: t.apply(point).y)
            label.layer.anchorPoint = CGPoint(x: 1, y: 0.5)
            label.textAlignment = .Right
        }
        label.center = q
        
        return label
    }
    
    func gridValuesForRange(min: CGFloat, max: CGFloat) -> [CGFloat] {
        var values = [CGFloat]()
        
        let base10 = floor(log10(max-min)-0.3)
        let gridSpacing = pow(10, base10)
        
        var value = ceil(min/gridSpacing)*gridSpacing
        
        while value < max {
            values.append(value)
            value += gridSpacing
        }
        
        return values
    }
    

    public func addLine(values: [CGFloat]) {
        var points = [CGPoint]()
        for (index, val) in values.enumerate() {
            points.append(CGPoint(x: CGFloat(index), y: val))
        }
        pointss.append(points)
    }
    
    public func addLine(points: [CGPoint]) {
        pointss.append(points)
    }

    public func addLines(pointss: [[CGPoint]]) {
        for points in pointss {
            addLine(points)
        }
    }
    
    public func clear() {
        axesMaxMinPathBox = CGRect(x: 0, y: 0, width: 0.00001, height: 0.00001)
        _ = self.subviews.map({ $0.removeFromSuperview() })
        pointss = [[CGPoint]]()
        shapeLayer.sublayers = nil
    }
    
    override public class func layerClass() -> AnyClass { return CAShapeLayer.self }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
    }
    
//    public convenience init () {
//        self.init(frame: CGRect(x: 0, y: 0, width: 500, height: 300))
//    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //        fatalError("init(coder:) has not been implemented")
    }
    
}
