//
// This file (and all other Swift source files in the Sources directory of this playground) will be precompiled into a framework which is automatically made available to AlgoDev.playground.
//

import UIKit



//
//  Extensions.swift
//  Feynan
//
//  Created by Gustaf Kugelberg on 24/04/15.
//  Copyright (c) 2015 GreatApe. All rights reserved.
//

func map<S, T>(f: S -> T) -> [S] -> [T] {
    return {
        return $0.map(f)
    }
}

infix operator |> { associativity left precedence 95 }

func |><S, T>(s: S, f: S -> T) -> T {
    return f(s)
}

public typealias Affine = CGAffineTransform

public extension UIBezierPath {
    public func transformed(t: Affine) -> UIBezierPath {
        let path = copy() as! UIBezierPath
        path.applyTransform(t)
        
        return path
    }
}


public extension CGAffineTransform {
    static var id: CGAffineTransform { return CGAffineTransformIdentity }
    
    init() {
        self = CGAffineTransform.id
    }
    
    public init(from: CGRect, to: CGRect, flipped: Bool = false) {
        let sx = to.width/from.width
        let sy = to.height/from.height
        
        if flipped {
            self = scaling(sx, sy: -sy)*translation(to.minX - from.minX*sx, y: to.maxY + from.minY*sy)
        }
        else {
            self = scaling(sx, sy: sy)*translation(to.minX - from.minX*sx, y: to.minY - from.minY*sy)
        }
    }
    
    init(to: CGRect, flipped: Bool = false) {
        self = CGAffineTransform(from: CGRect(x: 0, y: 0, width: 1, height:1), to: to, flipped: flipped)
    }
    
    func translate(x: CGFloat, _ y: CGFloat) -> CGAffineTransform {
        return CGAffineTransformTranslate(self, x, y)
    }
    
    func rotate(angle: CGFloat) -> CGAffineTransform {
        return CGAffineTransformRotate(self, angle)
    }
    
    func scale(x: CGFloat, _ y: CGFloat) -> CGAffineTransform {
        return CGAffineTransformScale(self, x, y)
    }
    
    func scale(r: CGFloat) -> CGAffineTransform {
        return scale(r, r)
    }
    
    func apply(rect: CGRect) -> CGRect {
        return CGRectApplyAffineTransform(rect, self)
    }
    
    func apply(point: CGPoint) -> CGPoint {
        return CGPointApplyAffineTransform(point, self)
    }
    
    var inverse: CGAffineTransform {
        return CGAffineTransformInvert(self)
    }
}

func translation(x: CGFloat, y: CGFloat) -> CGAffineTransform {
    return CGAffineTransformMakeTranslation(x, y)
}

func rotation(angle: CGFloat) -> CGAffineTransform {
    return CGAffineTransformMakeRotation(angle)
}

func scaling(sx: CGFloat, sy: CGFloat) -> CGAffineTransform {
    return CGAffineTransformMakeScale(sx, sy)
}

func scaling(r: CGFloat) -> CGAffineTransform {
    return scaling(r, sy: r)
}

func *(lhs: Affine, rhs: Affine) -> Affine {
    return CGAffineTransformConcat(lhs, rhs)
}

//extension CGPoint {
//    init(pos: Position) {
//        self.init(x: pos.latitude, y: pos.longitude)
//    }
//}

func norm(p: CGPoint) -> CGFloat {
    return sqrt(p.x*p.x + p.y*p.y)
}

func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

func -(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

extension CGRect {
    init(center: CGPoint, size: CGSize) {
        self.init(origin: CGPoint(x: center.x - size.width/2, y: center.y - size.height/2), size: size)
    }
    
    func grown(delta: CGFloat) -> CGRect {
        let dw = delta*width
        let dh = delta*height
        
        return CGRect(x: origin.x - dw, y: origin.y - dh, width: width + 2*dw, height: height + 2*dh)
    }
    
    func insetX(dx: CGFloat) -> CGRect {
        return CGRect(x: origin.x + dx, y: origin.y, width: width - 2*dx, height: height)
    }
    
    func insetY(dy: CGFloat) -> CGRect {
        return CGRect(x: origin.x, y: origin.y + dy, width: width, height: height - 2*dy)
    }
    
    func scaled(sx: CGFloat, _ sy: CGFloat, centered: Bool = false) -> CGRect {
        let newSize = CGSize(width: sx*width, height: sy*height)
        let newOrigin = centered ? CGPoint(x: minX - sx*width/2, y: minY - sy*height/2) : origin
        
        return CGRect(origin: newOrigin, size: newSize)
    }
    
    func scaledX(sx: CGFloat, centered: Bool = false) -> CGRect {
        return scaled(sx, 1, centered: centered)
    }
    
    func scaledY(sy: CGFloat, centered: Bool = false) -> CGRect {
        return scaled(1, sy, centered: centered)
    }
    
    func scaled(s: CGFloat, centered: Bool = false) -> CGRect {
        return scaled(s, s, centered: centered)
    }
    
    func moved(dr: CGPoint) -> CGRect {
        return CGRect(origin: origin + dr, size: size)
    }
    
    func moved(dx: CGFloat, _ dy: CGFloat) -> CGRect {
        return moved(CGPoint(x: dx, y: dy))
    }
    
    func movedX(dx: CGFloat) -> CGRect {
        return moved(CGPoint(x: dx, y: 0))
    }
    
    func movedY(dy: CGFloat) -> CGRect {
        return moved(CGPoint(x: 0, y: dy))
    }
    
    var center: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
    
    var midRight: CGPoint {
        return CGPoint(x: maxX, y: midY)
    }
    
    var lowerRight: CGPoint {
        return CGPoint(x: maxX, y: maxY)
    }
    
    var lowerCenter: CGPoint {
        return CGPoint(x: midX, y: maxY)
    }
    
    var lowerLeft: CGPoint {
        return CGPoint(x: minX, y: maxY)
    }
    
    var midLeft: CGPoint {
        return CGPoint(x: minX, y: midY)
    }
    
    var upperLeft: CGPoint {
        return CGPoint(x: minX, y: minY)
    }
    
    var upperCenter: CGPoint {
        return CGPoint(x: midX, y: minY)
    }
    
    var upperRight: CGPoint {
        return CGPoint(x: maxX, y: minY)
    }
    
    var aspect: CGFloat {
        return width/height
    }
}

extension CGSize {
    func scaled(sx: CGFloat, _ sy: CGFloat) -> CGSize {
        return CGSize(width: sx*width, height: sy*height)
    }
    
    func scaled(s: CGFloat) -> CGSize {
        return scaled(s, s)
    }
    
    func scaledX(sx: CGFloat) -> CGSize {
        return scaled(sx, 1)
    }
    
    func scaledY(sy: CGFloat) -> CGSize {
        return scaled(1, sy)
    }
}

func flipY(p: CGPoint) -> CGPoint {
    return CGPoint(x: p.x, y: -p.y)
}

//func dropFirst(list: [CGPoint]) -> [CGPoint] {
//    return CGPoint(x: p.x, y: -p.y)
//}

public func bezierPath(points: [CGPoint]) -> UIBezierPath {
    let path = UIBezierPath()
    
    if let first = points.first {
        path.moveToPoint(first)
        
        for point in points[1..<points.count] {
            path.addLineToPoint(point)
        }
    }
    
    return path
}